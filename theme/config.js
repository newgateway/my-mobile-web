const utils = require('@xdh/my/core/utils')
// 项目工程根目录
const ProjectRootPath = process.cwd()

// 临时文件存放绝对路径, 临时目录在工程下
const TempPath = utils.join(ProjectRootPath, '.my')

const VantThemeSrcPath = utils.join(ProjectRootPath, 'node_modules/vant/lib/style')

// VantUI 主题编译后存放目录
// const OutputVantThemePath = utils.join(TempPath, 'vant-themes')
const OutputVantThemePath = utils.join(ProjectRootPath, 'public/assets/vant-themes')

// 工程的主题配置LESS文件夹路径
const ProjectThemeVarPath = utils.join(ProjectRootPath, 'src/style/vant/themes')

const ThemeVarScssLoaderPath = utils.join(ProjectRootPath, 'node_modules/@xdh/my/core/loaders/theme-var-scss-loader.js')

module.exports = {
  ProjectRootPath,
  TempPath,
  VantThemeSrcPath,
  OutputVantThemePath,
  ProjectThemeVarPath,
  ThemeVarScssLoaderPath
}