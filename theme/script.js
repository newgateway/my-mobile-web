const builder = require('./pack/build')

builder((stats, ret) => {
  if (!ret) {
    console.log(stats.toString({
      chunks: false,
      colors: true
    }))
  }
})