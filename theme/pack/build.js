const config = require('../config')
const utils = require('@xdh/my/core/utils')
const webpack = require('webpack')
const {logWithSpinner, stopSpinner} = require('@vue/cli-shared-utils')
const fs = require('fs')

// webpack config 创建函数
const configFactory = require('./webpack-factory')

const SAVE_PATH = config.OutputVantThemePath

/**
 * 获取本地的主题配置文件
 * @return {{file: *, name: *}[]}
 */
function getLocalVarFiles() {
  const path = config.ProjectThemeVarPath
  const dirs = fs.readdirSync(path) || []

  return dirs.map((item) => {
    return {
      file: item,
      name: utils.kebabCase(item.replace('.less', ''))
    }
  })
}



// /**
//  * 获取样式源码目录
//  * @param type
//  * @return {string}
//  */
//  function getSrcPath(type) {}

 /**
 * 创建入口文件js文件，并写入scss引用， webpack4入口文件必须要js文件
 * @param type 框架类型了 ，el 或 ui
 * @param entryDir 入口文件存放目录
 * @return {*[]}
 */
/*
function createEntryFile(entryDir) {
  const themeSrcDir = config.VantThemeSrcPath
  if (!fs.existsSync(themeSrcDir)) {
    console.log('样式源码目录不存在')
    return []
  }
  const files = (fs.readdirSync(themeSrcDir) || []).filter(item => item.includes('.less'))

  files.forEach((item) => {
    const msg = utils.parsePath(item)
    const content = `import 'vant/lib/style/${msg.base}'`

    utils.writeFile(`${entryDir}/${msg.base.replace('.less', '.js')}`, content, 'utf-8')
  })
  
  return files
}
*/


// /**
//  * 构建入口的配置信息
//  * @param files 入口文件名数组
//  * @param entryDir 入口文件目录
//  */
/*
function getEntry(files, entryDir) {
  const entry = {}
  files.forEach(file => {
    const name = file.replace('.less', '')
    entry[name] = utils.join(entryDir, name + '.js')
  })
  return entry
}
*/

// /**
//  * 清理无用的文件
//  * @param {object} theme 主题描述对象
//  * @param {string} entryDir
//  */
 function clean(theme, entryDir) {
  const savePath = SAVE_PATH
  const buildDir = utils.join(savePath, theme.name, 'build')
  utils.rm(entryDir)
  utils.rm(buildDir)
 }
 
/**
 * 编译函数
 * @param type
 * @param themes
 * @param entryDir
 * @param callback
 */
function build(themes, entryDir, callback) {
  const theme = themes.pop()
  logWithSpinner(`编译vant主题【${theme.name}】`)

  // 创建入口文件
  // const files = createEntryFile(entryDir)

  // // 入口文件构造成webpack配置的entry对象
  // const entry = getEntry(files, entryDir)
  const entry = utils.join(config.ProjectRootPath, 'theme/static/entry.js')

  // 构建webpack配置
  const webpackConfig = configFactory(theme, entry, SAVE_PATH)

  webpack(webpackConfig, (err, stats) => {
    // 编译成功，清理无用文件
    clean(theme, entryDir)

    if (err || stats.hasErrors()) {
      callback && callback(stats, false)
    } else {
      stopSpinner()
      callback && callback(stats, true)
      if (themes.length > 0) {
        build(themes, entryDir, callback)
      }
    }
  })
}

module.exports = function (callback) {
  const savePath = SAVE_PATH
  const entryDir = utils.join(savePath, '.entry')

  // 获取项目主题文件
  const themes = getLocalVarFiles() || []

  if (themes.length > 0) {
    build(themes, entryDir, callback)
  } else {
    utils.log('主题配置文件', 'warning')
  }
}